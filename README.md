# projet_quechua

## Informations

Les wireframes sont accessibles ici : https://www.figma.com/file/OTIfPTYY3e5xFi9V8R3G0d/QUENOUILLERE---PROJECT-QUECHUA?node-id=0%3A1

## Avancée (Dernière update  : 03/12/21)

**--- ELEMENT BASIQUE | DESKTOP & MOBILE ---**

🟢 Head

🟡 Header
    
  => 🟡 Header Desktop (Prêt dans les grandes lignes, à styliser)
    
  => 🟡 Header responsive (Menu burger prêt, à remplir et styliser)
    
🟢 Footer

  => 🟢 Footer Desktop
    
  => 🟢 Footer responsive


**--- FRONT HTML & CSS | DESKTOP ---**

🟡 Page d’accueil (Squelette prêt, contenu prêt, manque ajout image)

🟢 Page de catégorie de produits

🟡 Page produit standard (Squelette en cours)


**--- FRONT HTML & CSS | MOBILE ---**

🟡 Page d’accueil (Squelette prêt, contenu prêt, manque ajout image)

🟢 Page de catégorie de produits

🟡 Page produit standard (Squelette en cours)


**--- JAVASCRIPT ---**

🟢 Classe *Produit*

  => 🟢 Display class product for the list of product into Product Category Page


🟡 Programmation des filtres

  => 🟢 Filtre Prix

  => 🟢 Filtre Niveaux

  => 🟡 Animation Type de produits

🟢 Product Category Page
 
  => 🟢 SideBar of the filter box

🟡 Header

  => 🟢 Animation Menu Burger

  => 🟢 Animation Blur 

  => 🟢 Animation Recherche autocomplétion

  => 🟢 Animation Disparition/Apparition du Header au scroll

  => 🔴 Animation Dropdown Menu 

🟢 Product page
 
  => 🟢 On mobile version, characteristics bloc, which change the content of the box depends on the content selected
