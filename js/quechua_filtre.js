/*Affichage produit*/
let cartes = listproduct;

let elcartes = document.getElementById('produits');


//function getClassFromBudget - N.QUENOUILLERE
function getClassFromBudget(p){
    if(p > 100.00){
        return "100above";
    } else if((p <= 100.00) && (p > 50.00)) {
        return "50100";
    } else if((p <= 50.00) && (p > 25.00)){
        return "2550";
    } else if((p <= 25.00) && (p > 10.00)){
        return "1025";
    } else if((p <= 10.00) && (p >= 0.00)){
        return "under10";
    }
    return "unknown";
}

//function getClassFromType - N.QUENOUILLERE
function getClassFromType(t){
    return t.join(" ");
}

//function getClassFromLevel - N.QUENOUILLERE
function getClassFromLevel(l){
    return levelArray[l];
}

function Rating(avis) {
    rate = '';
    switch (avis) {
        case 0.5:
        case 0.6:
        case 0.7:
        case 0.8:
        case 0.9:
            rate = `
          <i class="fas fa-star-half-alt"></i>
          <i class="far fa-star"></i>
          <i class="far fa-star"></i>
          <i class="far fa-star"></i>
          <i class="far fa-star"></i>
          `
            break;
        case 1.0:
        case 1.1:
        case 1.2:
        case 1.3:
        case 1.4:
            rate = `
          <i class="fas fa-star"></i>
          <i class="far fa-star"></i>
          <i class="far fa-star"></i>
          <i class="far fa-star"></i>
          <i class="far fa-star"></i>
          `
            break;
        case 1.5:
        case 1.6:
        case 1.7:
        case 1.8:
        case 1.9:
            rate = `
          <i class="fas fa-star"></i>
          <i class="fas fa-star-half-alt"></i>
          <i class="far fa-star"></i>
          <i class="far fa-star"></i>
          <i class="far fa-star"></i>
          `
            break;
        case 2.0:
        case 2.1:
        case 2.2:
        case 2.3:
        case 2.4:
            rate = `
          <i class="fas fa-star"></i>
          <i class="fas fa-star"></i>
          <i class="far fa-star"></i>
          <i class="far fa-star"></i>
          <i class="far fa-star"></i>
          `
            break;
        case 2.5:
        case 2.6:
        case 2.7:
        case 2.8:
        case 2.9:
            rate = `
          <i class="fas fa-star"></i>
          <i class="fas fa-star"></i>
          <i class="fas fa-star-half-alt"></i>
          <i class="far fa-star"></i>
          <i class="far fa-star"></i>
          `
            break;
        case 3.0:
        case 3.1:
        case 3.2:
        case 3.3:
        case 3.4:
            rate = `
          <i class="fas fa-star"></i>
          <i class="fas fa-star"></i>
          <i class="fas fa-star"></i>
          <i class="far fa-star"></i>
          <i class="far fa-star"></i>
          `
            break;
        case 3.5:
        case 3.6:
        case 3.7:
        case 3.8:
        case 3.9:
            rate = `
          <i class="fas fa-star"></i>
          <i class="fas fa-star"></i>
          <i class="fas fa-star"></i>
          <i class="fas fa-star-half-alt"></i>
          <i class="far fa-star"></i>
          `
            break;
        case 4.0:
        case 4.1:
        case 4.2:
        case 4.3:
        case 4.4:
            rate = `
              <i class="fas fa-star"></i>
              <i class="fas fa-star"></i>
              <i class="fas fa-star"></i>
              <i class="fas fa-star"></i>
              <i class="far fa-star"></i>
              `
            break;
        case 4.5:
        case 4.6:
        case 4.7:
        case 4.8:
        case 4.9:
            rate = `
                  <i class="fas fa-star"></i>
                  <i class="fas fa-star"></i>
                  <i class="fas fa-star"></i>
                  <i class="fas fa-star"></i>
                  <i class="fas fa-star-half-alt"></i>
                  `
            break;
        case 5.0:
            rate = `
                  <i class="fas fa-star"></i>
                  <i class="fas fa-star"></i>
                  <i class="fas fa-star"></i>
                  <i class="fas fa-star"></i>
                  <i class="fas fa-star"></i>
                  `
            break;
    }
    return rate;
}

let rating = [];
for (let i in cartes) {
    rating.push(Rating(cartes[i].avis))
}

// var carteFiltre = cartes.slice();

// var carteFiltrer = [];
// if (document.getElementById('under10').checked == true) {
//     return carteFiltrer = carteFiltre.filter((carte) => carte.niveau === 'pro');
// }
// let eltypes = document.getElementByClass('type');
// eltypes.textContent = cartes[1].niveau;



html = '';
for (let i in cartes) {
    html += `
<div class="col mb-5 all Pall Call ${getClassFromLevel(cartes[i].level)} ${getClassFromBudget(cartes[i].price)} ${getClassFromType(cartes[i].type)} produit">
<div class="card h-100 parent">
<img height="200em" class="card-img-top" src="img/articles/${cartes[i].img}" alt="${cartes[i].description}"/>
<div class="card-body p-4">
<div class="text-center">
<h5 class="fw-bolder text-black"> ${cartes[i].name} </h5>
</div>
<div class="text-center" >`
    cartes[i].rating = Rating(cartes[i].rating);
    html += `<button type="button" class="btn btn-default ">${cartes[i].rating}</button>`
    html += `
<p class="text-black">${cartes[i].price}€</p>
</div>
</div>
<div class="enfant">
<div class="row card-footer p-4 pt-0 border-top-0 bg-transparent">
<div class="col-6 text-left">
<a class="btn btn-outline-dark mt-auto main-btn" href="product.html"> Voir le produit </a>
</div>
<div class="col-6 text-right"> 
<a class="btn btn-outline-dark mt-auto secondary-btn" href="#"> Ajout au panier </a>
</div>
</div>
</div>
</div>
</div>`
}
console.log(html);
elcartes.innerHTML = html;


/*Filtre prix*/


var under101 = document.getElementsByClassName('under10');
var u10252 = document.getElementsByClassName('1025');
var u25503 = document.getElementsByClassName('2550');
var u501004 = document.getElementsByClassName('50100');
var u100above5 = document.getElementsByClassName('100above');
var utout6 = document.getElementsByClassName('Pall');


document.getElementById('Pall').onclick = function() {
    for (var i = 0; i < utout6.length; i++) {
        utout6[i].classList.remove('hidden');
    }
}

document.getElementById('under10').onclick = function() {
    for (var i = 0; i < u10252.length; i++) {
        u10252[i].classList.add('hidden');
    }
    for (var i = 0; i < u25503.length; i++) {
        u25503[i].classList.add('hidden');
    }
    for (var i = 0; i < u501004.length; i++) {
        u501004[i].classList.add('hidden');
    }
    for (var i = 0; i < under101.length; i++) {
        under101[i].classList.remove('hidden');
    }
    for (var i = 0; i < u100above5.length; i++) {
        u100above5[i].classList.add('hidden');
    }
}

document.getElementById('1025').onclick = function() {
    for (var i = 0; i < under101.length; i++) {
        under101[i].classList.add('hidden');
    }
    for (var i = 0; i < u25503.length; i++) {
        u25503[i].classList.add('hidden');
    }
    for (var i = 0; i < u501004.length; i++) {
        u501004[i].classList.add('hidden');
    }
    for (var i = 0; i < u10252.length; i++) {
        u10252[i].classList.remove('hidden');
    }
    for (var i = 0; i < u100above5.length; i++) {
        u100above5[i].classList.add('hidden');
    }
}

document.getElementById('2550').onclick = function() {
    for (var i = 0; i < under101.length; i++) {
        under101[i].classList.add('hidden');
    }
    for (var i = 0; i < u10252.length; i++) {
        u10252[i].classList.add('hidden');
    }
    for (var i = 0; i < u501004.length; i++) {
        u501004[i].classList.add('hidden');
    }
    for (var i = 0; i < u25503.length; i++) {
        u25503[i].classList.remove('hidden');
    }
    for (var i = 0; i < u100above5.length; i++) {
        u100above5[i].classList.add('hidden');
    }
}

document.getElementById('50100').onclick = function() {
    for (var i = 0; i < under101.length; i++) {
        under101[i].classList.add('hidden');
    }
    for (var i = 0; i < u10252.length; i++) {
        u10252[i].classList.add('hidden');
    }
    for (var i = 0; i < u25503.length; i++) {
        u25503[i].classList.add('hidden');
    }
    for (var i = 0; i < u501004.length; i++) {
        u501004[i].classList.remove('hidden');
    }
    for (var i = 0; i < u100above5.length; i++) {
        u100above5[i].classList.add('hidden');
    }
}

document.getElementById('100above').onclick = function() {
    for (var i = 0; i < u10252.length; i++) {
        u10252[i].classList.add('hidden');
    }
    for (var i = 0; i < u25503.length; i++) {
        u25503[i].classList.add('hidden');
    }
    for (var i = 0; i < u501004.length; i++) {
        u501004[i].classList.add('hidden');
    }
    for (var i = 0; i < under101.length; i++) {
        under101[i].classList.add('hidden');
    }
    for (var i = 0; i < u100above5.length; i++) {
        u100above5[i].classList.remove('hidden');
    }
}

/*Filtre Niveau*/


var debutant1 = document.getElementsByClassName('debutant');
var intermediaire2 = document.getElementsByClassName('intermediaire');
var expert3 = document.getElementsByClassName('expert');
var pro4 = document.getElementsByClassName('pro');
var tout5 = document.getElementsByClassName('all');

document.getElementById('all').onclick = function() {
    for (var i = 0; i < tout5.length; i++) {
        tout5[i].classList.remove('hidden');
    }
    document.getElementById('haut').innerHTML = '<h2>Tous les produits !</h2>';
}

document.getElementById('debutant').onclick = function() {
    for (var i = 0; i < intermediaire2.length; i++) {
        intermediaire2[i].classList.add('hidden');
    }
    for (var i = 0; i < expert3.length; i++) {
        expert3[i].classList.add('hidden');
    }
    for (var i = 0; i < pro4.length; i++) {
        pro4[i].classList.add('hidden');
    }
    for (var i = 0; i < debutant1.length; i++) {
        debutant1[i].classList.remove('hidden');
    }
    document.getElementById('haut').innerHTML = '<h2>Tous les produits pour les débutants !</h2>';
}

document.getElementById('intermediaire').onclick = function() {
    for (var i = 0; i < debutant1.length; i++) {
        debutant1[i].classList.add('hidden');
    }
    for (var i = 0; i < expert3.length; i++) {
        expert3[i].classList.add('hidden');
    }
    for (var i = 0; i < pro4.length; i++) {
        pro4[i].classList.add('hidden');
    }
    for (var i = 0; i < intermediaire2.length; i++) {
        intermediaire2[i].classList.remove('hidden');
    }
    document.getElementById('haut').innerHTML = '<h2>Tous les produits pour les niveaux intermédiaires !</h2>';
}

document.getElementById('expert').onclick = function() {
    for (var i = 0; i < debutant1.length; i++) {
        debutant1[i].classList.add('hidden');
    }
    for (var i = 0; i < intermediaire2.length; i++) {
        intermediaire2[i].classList.add('hidden');
    }
    for (var i = 0; i < pro4.length; i++) {
        pro4[i].classList.add('hidden');
    }
    for (var i = 0; i < expert3.length; i++) {
        expert3[i].classList.remove('hidden');
    }
    document.getElementById('haut').innerHTML = '<h2>Tous les produits pour les experts !</h2>';
}

document.getElementById('pro').onclick = function() {
    for (var i = 0; i < debutant1.length; i++) {
        debutant1[i].classList.add('hidden');
    }
    for (var i = 0; i < intermediaire2.length; i++) {
        intermediaire2[i].classList.add('hidden');
    }
    for (var i = 0; i < expert3.length; i++) {
        expert3[i].classList.add('hidden');
    }
    for (var i = 0; i < pro4.length; i++) {
        pro4[i].classList.remove('hidden');
    }
    document.getElementById('haut').innerHTML = '<h2>Tous les produits pour les pros !</h2>';
}


/* Types de produits */

var ctente1 = document.getElementsByClassName('Tente');
var ctable2 = document.getElementsByClassName('Table');
var ckway3 = document.getElementsByClassName('Kway');
var cmatelas4 = document.getElementsByClassName('Matelas');
var ccouverture5 = document.getElementsByClassName('Couverture');
var coreiller6 = document.getElementsByClassName('Oreiller');
var ckit7 = document.getElementsByClassName('Kit');
var cpiquet8 = document.getElementsByClassName('Piquet');
var ctout9 = document.getElementsByClassName('Call');

document.getElementById('Tente').onclick = function() {
    for (var i = 0; i < ctente1.length; i++) {
        ctente1[i].classList.remove('hidden');
    }
    for (var i = 0; i < ctable2.length; i++) {
        ctable2[i].classList.add('hidden');
    }
    for (var i = 0; i < ckway3.length; i++) {
        ckway3[i].classList.add('hidden');
    }
    for (var i = 0; i < cmatelas4.length; i++) {
        cmatelas4[i].classList.add('hidden');
    }
    for (var i = 0; i < ccouverture5.length; i++) {
        ccouverture5[i].classList.add('hidden');
    }
    for (var i = 0; i < coreiller6.length; i++) {
        coreiller6[i].classList.add('hidden');
    }
    for (var i = 0; i < ckit7.length; i++) {
        ckit7[i].classList.add('hidden');
    }
    for (var i = 0; i < cpiquet8.length; i++) {
        cpiquet8[i].classList.add('hidden');
    }
}
document.getElementById('Table').onclick = function() {
    for (var i = 0; i < ctable2.length; i++) {
        ctable2[i].classList.remove('hidden');
    }
    for (var i = 0; i < ctente1.length; i++) {
        ctente1[i].classList.add('hidden');
    }
    for (var i = 0; i < ckway3.length; i++) {
        ckway3[i].classList.add('hidden');
    }
    for (var i = 0; i < cmatelas4.length; i++) {
        cmatelas4[i].classList.add('hidden');
    }
    for (var i = 0; i < ccouverture5.length; i++) {
        ccouverture5[i].classList.add('hidden');
    }
    for (var i = 0; i < coreiller6.length; i++) {
        coreiller6[i].classList.add('hidden');
    }
    for (var i = 0; i < ckit7.length; i++) {
        ckit7[i].classList.add('hidden');
    }
    for (var i = 0; i < cpiquet8.length; i++) {
        cpiquet8[i].classList.add('hidden');
    }
}
document.getElementById('Kway').onclick = function() {
    for (var i = 0; i < ckway3.length; i++) {
        ckway3[i].classList.remove('hidden');
    }
    for (var i = 0; i < ctente1.length; i++) {
        ctente1[i].classList.add('hidden');
    }
    for (var i = 0; i < ctable2.length; i++) {
        ctable2[i].classList.add('hidden');
    }
    for (var i = 0; i < ckway3.length; i++) {
        ckway3[i].classList.add('hidden');
    }
    for (var i = 0; i < cmatelas4.length; i++) {
        cmatelas4[i].classList.add('hidden');
    }
    for (var i = 0; i < ccouverture5.length; i++) {
        ccouverture5[i].classList.add('hidden');
    }
    for (var i = 0; i < coreiller6.length; i++) {
        coreiller6[i].classList.add('hidden');
    }
    for (var i = 0; i < ckit7.length; i++) {
        ckit7[i].classList.add('hidden');
    }
    for (var i = 0; i < cpiquet8.length; i++) {
        cpiquet8[i].classList.add('hidden');
    }
}
document.getElementById('Matelas').onclick = function() {
    for (var i = 0; i < cmatelas4.length; i++) {
        cmatelas4[i].classList.remove('hidden');
    }
    for (var i = 0; i < ctente1.length; i++) {
        ctente1[i].classList.add('hidden');
    }
    for (var i = 0; i < ctable2.length; i++) {
        ctable2[i].classList.add('hidden');
    }
    for (var i = 0; i < ckway3.length; i++) {
        ckway3[i].classList.add('hidden');
    }
    for (var i = 0; i < ccouverture5.length; i++) {
        ccouverture5[i].classList.add('hidden');
    }
    for (var i = 0; i < coreiller6.length; i++) {
        coreiller6[i].classList.add('hidden');
    }
    for (var i = 0; i < ckit7.length; i++) {
        ckit7[i].classList.add('hidden');
    }
    for (var i = 0; i < cpiquet8.length; i++) {
        cpiquet8[i].classList.add('hidden');
    }
}
document.getElementById('Couverture').onclick = function() {
    for (var i = 0; i < ccouverture5.length; i++) {
        ccouverture5[i].classList.remove('hidden');
    }
    for (var i = 0; i < ctente1.length; i++) {
        ctente1[i].classList.add('hidden');
    }
    for (var i = 0; i < ctable2.length; i++) {
        ctable2[i].classList.add('hidden');
    }
    for (var i = 0; i < ckway3.length; i++) {
        ckway3[i].classList.add('hidden');
    }
    for (var i = 0; i < cmatelas4.length; i++) {
        cmatelas4[i].classList.add('hidden');
    }
    for (var i = 0; i < coreiller6.length; i++) {
        coreiller6[i].classList.add('hidden');
    }
    for (var i = 0; i < ckit7.length; i++) {
        ckit7[i].classList.add('hidden');
    }
    for (var i = 0; i < cpiquet8.length; i++) {
        cpiquet8[i].classList.add('hidden');
    }
}
document.getElementById('Oreiller').onclick = function() {
    for (var i = 0; i < coreiller6.length; i++) {
        coreiller6[i].classList.remove('hidden');
    }
    for (var i = 0; i < ctente1.length; i++) {
        ctente1[i].classList.add('hidden');
    }
    for (var i = 0; i < ctable2.length; i++) {
        ctable2[i].classList.add('hidden');
    }
    for (var i = 0; i < ckway3.length; i++) {
        ckway3[i].classList.add('hidden');
    }
    for (var i = 0; i < cmatelas4.length; i++) {
        cmatelas4[i].classList.add('hidden');
    }
    for (var i = 0; i < ccouverture5.length; i++) {
        ccouverture5[i].classList.add('hidden');
    }
    for (var i = 0; i < ckit7.length; i++) {
        ckit7[i].classList.add('hidden');
    }
    for (var i = 0; i < cpiquet8.length; i++) {
        cpiquet8[i].classList.add('hidden');
    }
}
document.getElementById('Kit').onclick = function() {
    for (var i = 0; i < ckit7.length; i++) {
        ckit7[i].classList.remove('hidden');
    }
    for (var i = 0; i < ctente1.length; i++) {
        ctente1[i].classList.add('hidden');
    }
    for (var i = 0; i < ctable2.length; i++) {
        ctable2[i].classList.add('hidden');
    }
    for (var i = 0; i < ckway3.length; i++) {
        ckway3[i].classList.add('hidden');
    }
    for (var i = 0; i < cmatelas4.length; i++) {
        cmatelas4[i].classList.add('hidden');
    }
    for (var i = 0; i < ccouverture5.length; i++) {
        ccouverture5[i].classList.add('hidden');
    }
    for (var i = 0; i < coreiller6.length; i++) {
        coreiller6[i].classList.add('hidden');
    }
    for (var i = 0; i < cpiquet8.length; i++) {
        cpiquet8[i].classList.add('hidden');
    }
}
document.getElementById('Piquet').onclick = function() {
    for (var i = 0; i < cpiquet8.length; i++) {
        cpiquet8[i].classList.remove('hidden');
    }
    for (var i = 0; i < ctente1.length; i++) {
        ctente1[i].classList.add('hidden');
    }
    for (var i = 0; i < ctable2.length; i++) {
        ctable2[i].classList.add('hidden');
    }
    for (var i = 0; i < ckway3.length; i++) {
        ckway3[i].classList.add('hidden');
    }
    for (var i = 0; i < cmatelas4.length; i++) {
        cmatelas4[i].classList.add('hidden');
    }
    for (var i = 0; i < ccouverture5.length; i++) {
        ccouverture5[i].classList.add('hidden');
    }
    for (var i = 0; i < coreiller6.length; i++) {
        coreiller6[i].classList.add('hidden');
    }
    for (var i = 0; i < ckit7.length; i++) {
        ckit7[i].classList.add('hidden');
    }
}



/*Affichage sidebar*/



$(document).ready(function() {
    $('#sidebarCollapse').on('click', function() {
        $('#sidebar').toggleClass('active');
        $(this).toggleClass('active');
    });
});
