if (window.scrollY) {
    window.scroll(0, 0);  // reset the scroll position to the top left of the document.
}
document.addEventListener("DOMContentLoaded", function(){
    window.scroll("0","120");

    array_mobile_info = ["description","caracteristique","impact"];

    array_mobile_info.forEach(className => {
        document.getElementById("m_"+className).onclick = function () {
            array_mobile_info.forEach(classNameChange => {
                if(className == classNameChange) {
                    document.getElementById("m_"+classNameChange).classList.add("m_selected");
                    document.getElementById("m_"+classNameChange+"_content").classList.remove("hidden");
                } else {
                    document.getElementById("m_"+classNameChange).classList.remove("m_selected");
                    document.getElementById("m_"+classNameChange+"_content").classList.add("hidden");
                }
            });
        }    
    });

});