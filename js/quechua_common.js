document.addEventListener("DOMContentLoaded", function(){
    var blurTimeout = null;
    var blurOn = false;
    var searchLock = false;
    var oldSizeY = 0;
    var burger_menu = document.getElementById("burger_icon");
    var sidemenu = document.getElementById("sidemenu");
    var bg_blur = document.getElementById("sidemenu_blur");
    var searchBox = document.getElementById("searchBox");
    var autocompletion = document.getElementById("autocompletion");
    var viewMode = "desktop";

    if(document.querySelector("body").offsetWidth<768){
        viewMode = "mobile";
    }
    burger_menu.addEventListener("click", function () {
        clearTimeout(blurTimeout);
        if (burger_menu.getAttribute("data-open") == "false") {
            burger_menu.setAttribute("data-open", "true");
            burger_menu.classList.add("fa-chevron-right");
            burger_menu.classList.remove("fa-bars");

            sidemenu.classList.remove("animation_close_sidemenu");
            void sidemenu.offsetWidth;
            sidemenu.classList.add("animation_open_sidemenu");
            bg_blur.style.display = "block";
            bg_blur.classList.remove("animation_desactive_bg_blur");
            void bg_blur.offsetWidth;
            bg_blur.classList.add("animation_active_bg_blur");
        } else {
            burger_menu.setAttribute("data-open", "false");
            burger_menu.classList.add("fa-bars");
            burger_menu.classList.remove("fa-chevron-right");

            
            sidemenu.classList.remove("animation_open_sidemenu");
            void sidemenu.offsetWidth;
            sidemenu.classList.add("animation_close_sidemenu");

            bg_blur.classList.remove("animation_active_bg_blur");
            void bg_blur.offsetWidth;
            bg_blur.classList.add("animation_desactive_bg_blur");
            
        }
        burger_menu.classList.remove("animation_rotate_icon");
        void burger_menu.offsetWidth;
        burger_menu.classList.add("animation_rotate_icon");
    })

    searchBox.addEventListener('focus', (event) => {

        clearTimeout(blurTimeout);
        searchLock = true;
        bg_blur.style.display = "block";
        bg_blur.classList.remove("animation_desactive_bg_blur");
        void bg_blur.offsetWidth;
        bg_blur.classList.add("animation_active_bg_blur");
        detectSearchBar();
    });

    searchBox.addEventListener("keyup", (event) => {
        detectSearchBar();
    })

    searchBox.addEventListener('blur', (event) => {
        clearTimeout(blurTimeout);
        searchLock = false;
        setTimeout(function(){ 
            autocompletion.style.display = "none";
            if (burger_menu.getAttribute("data-open") == "false") {
                
                bg_blur.classList.remove("animation_active_bg_blur");
                void bg_blur.offsetWidth;
                bg_blur.classList.add("animation_desactive_bg_blur");
                
                blurTimeout = setTimeout(function(){ 
                    bg_blur.style.display = "none";
                }, 500);
            }
        }, 100);
        
    });

    window.addEventListener ("resize", function(){
        if(document.querySelector("body").offsetWidth<768){
            viewMode = "mobile";
        } else {
            if(viewMode = "mobile"){
                if (burger_menu.getAttribute("data-open") == "true") {
                    bg_blur.classList.remove("animation_active_bg_blur","animation_desactive_bg_blur");
                    bg_blur.style.opacity = 0;
                    bg_blur.style.display = "none";
                }
                burger_menu.setAttribute("data-open","false");
                sidemenu.classList.remove("animation_open_sidemenu","animation_close_sidemenu");
                burger_menu.classList.add("fa-bars");
                burger_menu.classList.remove("fa-chevron-right");
            }
            viewMode = "desktop";
        }
    },false);

    const body = document.body;
    const nav = document.querySelector("header");
    const scrollUp = "scroll-up";
    const scrollDown = "scroll-down";
    let lastScroll = -1;
    
    window.addEventListener("scroll", () => {
        //Don't fire scroll event in these case
        if((burger_menu.getAttribute("data-open") == "true") || searchLock || (oldSizeY != body.clientHeight)){
            oldSizeY = body.clientHeight;
            return;
        }
        const currentScroll = window.pageYOffset;
            
        if(lastScroll == -1){
            lastScroll = currentScroll;
            return;
        }
        
        if (currentScroll <= 0) {
            body.classList.remove(scrollUp);
            return;
        }
        
        if (currentScroll > lastScroll && !body.classList.contains(scrollDown)) {
            //Scroll down
            body.classList.remove(scrollUp);
            body.classList.add(scrollDown);
            nav.classList.remove("animation_open_navmenu");
            nav.pageYOffset;
            nav.classList.add("animation_close_navmenu");
        } else if (currentScroll < lastScroll && body.classList.contains(scrollDown)) {
            //Scroll up
            body.classList.remove(scrollDown);
            body.classList.add(scrollUp);
            nav.classList.remove("animation_close_navmenu");
            nav.pageYOffset;
            nav.classList.add("animation_open_navmenu");
            
        }
        lastScroll = currentScroll;
    });


    function generateAutoCompletion(searchText){
        var ul = document.getElementById("autocompletion_list");
        ul.innerHTML = "";
        listproduct.forEach(item => {
            if(item.name.toLowerCase().includes(searchText.toLowerCase())){
                let li_item = createAutoCompletionElement(item);
                ul.appendChild(li_item);
            }
        })
    }

    // Template LI item
    // <li>
    //     <div class="autocompletion_item" style="display:flex;">
    //         <div style="width:30%; background: center / contain no-repeat url(./img/articles/boots1.jpg);"></div>
    //         <div style="width:70%;">
    //             <h4>Bottes de pluie</h4>
    //             <div class="price_stock">
    //                 <h4>4.00€</h4>
    //                 <h4>En stock</h4>
    //             </div>
                
    //         </div>
    //     </div>
    // </li>
    function createAutoCompletionElement(item){
        //Create li element
        let tmp_li = document.createElement("li");
        //Create div item
        let tmp_div_item =document.createElement("div");
        tmp_div_item.classList.add("autocompletion_item");
        //Create div product image
        let tmp_div_image =document.createElement("div");
        tmp_div_image.style.width = "30%";
        tmp_div_image.style.background = "center / contain no-repeat url(./img/articles/"+item.img+")";
        tmp_div_item.appendChild(tmp_div_image);
        //Create div info
        let tmp_div_info = document.createElement("div");
        tmp_div_info.style.width = "70%";
        //Create H4 name
        let h4_name= document.createElement("h4");
        h4_name.innerHTML = item.name;
        tmp_div_info.appendChild(h4_name);
        //Create div price_stock
        let tmp_div = document.createElement("div");
        tmp_div.classList.add("price_stock");
        let h4_price = document.createElement("h4");
        h4_price.innerHTML = parseFloat(item.price).toFixed(2) + "€";
        let h4_stock = document.createElement("h4");
        if(item.stock > 0){
            h4_stock.innerHTML = "En stock";
            h4_stock.classList.add("stock");
        } else {
            h4_stock.innerHTML = "Réappro.";
            h4_stock.classList.add("nostock");
        }
        tmp_div.appendChild(h4_price);
        tmp_div.appendChild(h4_stock);
        tmp_div_info.appendChild(tmp_div);
        tmp_div_item.appendChild(tmp_div_info);
        tmp_div_item.setAttribute("onclick","window.location.href = './product.html';");
        //Add total div to to li
        tmp_div_item.style.cursor = "pointer";
        tmp_li.appendChild(tmp_div_item);
        //Add item to ul list
        return tmp_li;
    }

    function detectSearchBar(){
        if (searchBox.value.length > 0){
            generateAutoCompletion(searchBox.value);
            autocompletion.style.display = "flex";
        } else {
            autocompletion.style.display = "none";
        }
    }

});

class Produit {
    static countId = 0;
    id = -1;
    name = "";
    price = "";
    img = "blank.png";
    description = "";
    level = 0; //0:Débutant;1:Intermédiaire;2:Expert;3:Pro
    rating = 0.0;
    stock = 0;
    type = 0;

    constructor(name, price, img, description,level,rating,stock,type) {
      this.id = this.constructor.countId;
      this.constructor.countId = this.constructor.countId+1;
      this.name = name;
      this.price = price;
      this.img = img;
      this.description = description;
      this.level = level;
      this.rating = rating;
      this.stock = stock;
      this.type = type;
    }
}



function goToIndex(){
    window.location.href = './index.html';
}


